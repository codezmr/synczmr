+++
title = "About me"

[extra]
skip-meta = true
+++

# Who am I?

I am <span style="color:rgb(35,176,255)">**MOHAMMAD ZAMIRUDDIN**</span>, a Java developer with experience in full-stack development, primarily focusing on the Spring Framework.

