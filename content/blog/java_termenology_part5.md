+++
title = "JAVA Standard Terminology (Part-5)"
date = 2024-03-17

[taxonomies]
series=["JAVA Terminology Series"]
tags=["Java", "architectural concepts","development practices","design patterns"]
+++



## Java Standard Terminology
Here are more technical terms commonly used in the **Java development environment**.

These terms cover a variety of <ins>**design patterns**</ins>, <ins>**architectural concepts**</ins>, <ins>**development practices**</ins>, and tools commonly used in Java development and the broader software engineering field. Understanding these terms will contribute to your proficiency as a Java developer.

- **Composite Pattern:** `A structural pattern where objects are composed into tree structures to represent part-whole hierarchies.`

- **Command Pattern:** `A behavioral design pattern where an object represents a request and encapsulates all the details of the request, including the method call, the method arguments, and the receiving object.`

- **Factory Method Pattern:** `A creational pattern that defines an interface for creating an object but lets subclasses alter the type of objects that will be created.`

- **Singleton Pattern:** `A creational pattern that restricts the instantiation of a class to a single instance and provides a global point of access to that instance.`

- **Prototype Pattern:** `A creational pattern that specifies the kinds of objects to create using a prototypical instance, and creates new objects by copying this prototype.`

- **Adapter Pattern:** `A structural pattern that allows incompatible interfaces to work together. It acts as a bridge between two incompatible interfaces.`

- **Bridge Pattern:** `A structural pattern that separates an object's abstraction from its implementation so that the two can vary independently.`

- **Visitor Pattern:** `A behavioral design pattern where a visitor class is used to change the class of an element during runtime.`

- **Mediator Pattern:** `A behavioral design pattern that defines an object that centralizes communication between objects in a system.`

- **Strategy Pattern:** `A behavioral design pattern that defines a family of algorithms, encapsulates each algorithm, and makes the algorithms interchangeable.`

- **Command Line Interface (CLI):** `A user interface to a computer's operating system or an application in which the user responds to a visual prompt by typing a command on the keyboard.`

- **Batch Processing:** `The execution of a series of programs or jobs on a computer without manual intervention.`

- **Service-Oriented Architecture (SOA):** `An architectural pattern in software design where software components provide services to other components via a communication protocol over a network.`

- **Microservices:** `An architectural style that structures an application as a collection of small, independently deployable services.`

- **Containerization:** `A lightweight, portable, and self-sufficient unit that can run applications and their dependencies in isolated environments.`

- **Continuous Integration (CI):** `A development practice that requires developers to integrate code into a shared repository several times a day.`

- **Continuous Delivery (CD):** `An extension of continuous integration that enables automated testing and deployment of code to production environments.`

- **DevOps:** `A set of practices that combines software development (Dev) and IT operations (Ops), aiming to shorten the development lifecycle and deliver high-quality software continuously.`

- **Code Review:** `A systematic examination of source code to find and fix errors, improve the overall code quality, and ensure adherence to coding standards.`

- **Code Refactoring:** `The process of restructuring existing computer code without changing its external behavior. It is done to improve code readability, maintainability, and efficiency.`
