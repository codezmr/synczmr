+++
title = "JAVA Standard Terminology (Part-1)"
date = 2024-03-17

[taxonomies]
series=["JAVA Terminology Series"]
tags=["Java"]
+++

## Java Standard Terminology
Using precise and standard terminology in the Java development environment is important for effective communication and collaboration. Here are some common technical terms used in Java.


- **Class:** `A blueprint for creating objects. It defines the properties and behaviors that objects of the class will have.`

- **Object:** `An instance of a class. Objects are created from classes and represent real-world entities in your program.`

- **Method:** `A function that is associated with an object or class. Methods define the behavior of objects.`

- **Instance:** `A specific occurrence of an object created from a class. "Instance variable" refers to a variable that belongs to an instance of a class.`

```java
// Define a simple class called Car
class Car {
    // Instance variables (belong to each instance of the Car class)
    String model;
    int year;
    
    // Constructor to initialize the instance variables
    public Car(String model, int year) {
        this.model = model;
        this.year = year;
    }
    
    // Instance method to display information about the car
    public void displayInfo() {
        System.out.println("Model: " + model);
        System.out.println("Year: " + year);
    }
}

public class CarExample {
    public static void main(String[] args) {
        // Creating instances (specific occurrences) of the Car class
        Car car1 = new Car("Toyota Camry", 2022);
        Car car2 = new Car("Honda Accord", 2021);

        // Accessing and displaying information using instance methods
        System.out.println("Car 1 Details:");
        car1.displayInfo();

        System.out.println("\nCar 2 Details:");
        car2.displayInfo();
    }
}

```

- **Constructor:** `A special method used for initializing objects. It is called when an object is created.`

- **Inheritance:** `A mechanism in Java where a class can inherit the properties and behaviors of another class. It promotes code reuse and establishes a relationship between classes.`

- **Polymorphism:** `The ability of a method to do different things based on the object it is acting upon. This includes method overloading and method overriding.`

- **Encapsulation:** `The bundling of data (variables) and methods that operate on the data into a single unit (class). It helps in hiding the implementation details.`

- **Abstraction:** `The concept of simplifying complex systems by modeling classes based on the essential properties and behaviors they share.`

- **Interface:** `A collection of abstract methods. It defines a contract for classes that implement it, specifying what methods they must provide.`

- **Polymorphic Variable:** `A variable that can reference objects of different types. It is based on polymorphism, allowing flexibility in the type of objects it can hold.`

- **Exception:** `An event that disrupts the normal flow of a program. Exceptions can be caught and handled using try-catch blocks.`

- **Package:** `A mechanism for organizing classes into namespaces. It helps in avoiding naming conflicts and provides a way to group related classes.`

- **Static:** `A keyword indicating that a member (variable or method) belongs to the class rather than to instances of the class. It is shared among all instances.`

- **Abstract Class:** `A class that cannot be instantiated on its own and may contain abstract methods. It is meant to be subclassed.`

- **Polymorphic Method:** `A method that can take different forms depending on the object it acts upon. It involves method overriding.`

- **Getter and Setter:** `Methods used to retrieve (get) and modify (set) the values of private variables in a class, promoting encapsulation.`

- **Final Keyword:** `A keyword used to apply restrictions on class, method, and variable. For example, a final class cannot be subclassed, a final method cannot be overridden, and a final variable cannot be modified.`

- **Overloading:** `Defining multiple methods in a class with the same name but different parameters. It is a form of polymorphism.`

- **Overriding:** `Providing a specific implementation for a method in a subclass that is already defined in its superclass. It is a form of polymorphism.`
