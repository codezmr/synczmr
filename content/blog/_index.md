+++
title = "The Blog"
sort_by = "date"
paginate_by = 10
insert_anchor_links = "left"
generate_feed = true

[extra]
feed_discovery = true
+++

# Welcome to Threads of Blogs! 📚

* Explore the 🗄️ {{ url(path="/archive/", text="Archives") }}
* Discover all 📚 {{ url(path="/series/", text="Series") }}
* Dive into the 🏷️ {{ url(path="/tags/", text="Tags") }}
* Stay connected with the 📰 {{ url(path="blog/rss.xml", text="RSS feed") }}
