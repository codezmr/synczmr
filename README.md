# Sync ZMR - Profile

[Website](https://synczmr-codezmr-31a7267deec9b3f43ec8380add454cf2d8f267a9c544d5f.gitlab.io)

## About

This is the personal website of Mohammad Zamiruddin, also known as ZMR. The website showcases blog posts, projects, and information about ZMR.

## Credits

- [Zola](https://www.getzola.org/) - Static site generator used to build this website
- [Zerm Theme](https://github.com/ejmg/zerm) - Theme used for this website, designed by ejmg

